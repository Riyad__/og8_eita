package rekursion;

public interface IMyMath {
	
	int fakultaet(int zahl);
	
	int fibonacci(int zahl);
}