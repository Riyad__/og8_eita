package rekursion;

import java.util.Scanner;

public class MyMath implements IMyMath {

	@Override
	public int fakultaet(int zahl) {
		int fakultaet = 1;
		for (int i = 1; i <= zahl; i++) {
			fakultaet = fakultaet * i;
		}
		System.out.println("Die Fakult�t von " + zahl + " ist: " + fakultaet);

		return fakultaet;
	}

	@Override
	public int fibonacci(int zahl) {
		if (zahl == 0)
			return 0;
		else if (zahl == 1)
			return 1;
		else

			return fibonacci(zahl - 1) + fibonacci(zahl - 2);

	}

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		MyMath z = new MyMath();

		System.out.println("Geben Sie eine Zahl ein:");

		int zahl = eingabe.nextInt();

		z.fakultaet(zahl);
		System.out.println("Die eingegebene Fibonacci-Zahl lautet:" + z.fibonacci(zahl));

	}

}
