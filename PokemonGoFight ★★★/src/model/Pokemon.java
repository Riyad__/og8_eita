package model;

import model.Attacke;
import java.io.File;
import java.time.LocalDate;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 20.04.2020
 * @author Riyad
 */

public class Pokemon {

	// Anfang Attribute
	private int nummer;
	private String name;
	private File bild;
	private int wp;
	private int kp;
	private int maxkp;
	private boolean favorit;
	private String typ;
	private double groesse;
	private double gewicht;
	private int bonbons;
	private Attacke[] attacken;
	private LocalDate funddatum;
	// Ende Attribute

	public Pokemon() {
		this.nummer = 0;
		this.name = "";
		this.bild = null;
		this.wp = 0;
		this.kp = 0;
		this.maxkp = 0;
		this.favorit = false;
		this.typ = "";
		this.groesse = 0;
		this.gewicht = 0;
		this.bonbons = 0;
		this.attacken = null;
		this.funddatum = null;
	}

	public Pokemon(int nummer, String name, File bild, int wp, int kp, int maxkp, boolean favorit, String typ,
			double gewicht, double groesse, Attacke[] attacken, LocalDate funddatum) {
		this.nummer = nummer;
		this.name = name;
		this.bild = bild;
		this.wp = wp;
		this.kp = kp;
		this.maxkp = maxkp;
		this.favorit = favorit;
		this.typ = typ;
		this.groesse = groesse;
		this.gewicht = gewicht;
		this.funddatum = funddatum;
		this.bonbons = 0;
		this.attacken = null;
	}

	// Anfang Methoden
	public int getNummer() {
		return nummer;
	}

	public String getName() {
		return name;
	}

	public File getBild() {
		return bild;
	}

	public int getWp() {
		return wp;
	}

	public String getTyp() {
		return typ;
	}

	public int getKp() {
		return kp;
	}

	public int getMaxkp() {
		return maxkp;
	}

	public boolean getFavorit() {
		return favorit;
	}

	public double getGroesse() {
		return groesse;
	}

	public double getGewicht() {
		return gewicht;
	}

	public int getBonbons() {
		return bonbons;
	}

	public Attacke[] getAttacken() {
		return attacken;
	}

	public LocalDate getFunddatum() {
		return funddatum;
	}

	public void setNummer(int nummerNeu) {
		nummer = nummerNeu;
	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public void setBild(File bildNeu) {
		bild = bildNeu;
	}

	public void setWp(int wpNeu) {
		wp = wpNeu;
	}

	public void setKp(int kpNeu) {
		kp = kpNeu;
	}

	public void setMaxkp(int maxkpNeu) {
		maxkp = maxkpNeu;
	}

	public void setFavorit(boolean favoritNeu) {
		favorit = favoritNeu;
	}

	public void setTyp(String typNeu) {
		typ = typNeu;
	}

	public void setGroesse(double groesseNeu) {
		groesse = groesseNeu;
	}

	public void setGewicht(double gewichtNeu) {
		gewicht = gewichtNeu;
	}

	public void setBonbons(int bonbonsNeu) {
		bonbons = bonbonsNeu;
	}

	public void setAttacken(Attacke[] attackenNeu) {
		attacken = attackenNeu;
	}

	public void setFunddatum(LocalDate funddatumNeu) {
		funddatum = funddatumNeu;
	}

	// Ende Methoden
} // end of Pokemon
