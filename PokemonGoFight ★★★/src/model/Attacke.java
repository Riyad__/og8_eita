package model;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 20.04.2020
 * @author Riyad
 */

public class Attacke {

	// Anfang Attribute
	private String attackenname;
	private int schaden;
	// Ende Attribute

	public void Attacke(String attackenname, int schaden) {
		this.attackenname = attackenname;
		this.schaden = schaden;
	}

	// Anfang Methoden
	public String getAttackenname() {
		return attackenname;
	}

	public int getSchaden() {
		return schaden;
	}

	public void setAttackenname(String attackennameNeu) {
		attackenname = attackennameNeu;
	}

	public void setSchaden(int schadenNeu) {
		schaden = schadenNeu;
	}

	// Ende Methoden
} // end of Attacke
